export const enum PlayerEffect {
	None = 0,
	Leader = 1,
	Fight = 2,
	LevelUp = 4,
	Follow = 8,
	Healed = 16,
	Shop = 32,
	Red = 64,
	Blue = 128,
}
