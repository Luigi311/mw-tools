import { Logger } from '../Logger/Logger';
import { ActionIdExecutable } from './Actions/ActionIdExecutable';
import { ActionTemplateExecutable } from './Actions/ActionTemplateExecutable';
import { AddExpExecutable } from './Actions/AddExpExecutable';
import { AddGoldExecutable } from './Actions/AddGoldExecutable';
import { AddMonsterExecutable } from './Actions/AddMonsterExecutable';
import { AddItemExecutable } from './Actions/AddItemExecutable';
import { ArrayExecutable } from './Actions/ArrayExecutable';
import { HealExecutable } from './Actions/HealExecutable';
import { NpcSayExecutable } from './Actions/NpcSayExecutable';
import { QuestExecutable } from './Actions/QuestExecutable';
import { RemoveItemExecutable } from './Actions/RemoveItemExecutable';
import { TeleportExecutable } from './Actions/TeleportExecutable';
import type { ClientActionContext } from './GameActionContext';
import { GameActionExecutable } from './GameActionExecutable';
import type { GameAction } from './GameActionTypes';
import { AddPetExecutable } from './Actions/AddPetExecutable';

export abstract class GameActionParser {
	/**
	 * Turn a json action into an executable.
	 * @param action
	 */
	public static parse(action?: GameAction | null): GameActionExecutable<ClientActionContext> {
		if (!action) return GameActionExecutable.noop;

		if (typeof action === 'string')
			action = {
				type: 'id',
				id: action,
			};

		if (Array.isArray(action)) {
			action = {
				type: 'array',
				actions: action,
			};
		}

		switch (action.type) {
			case 'noop':
				return GameActionExecutable.noop;
			case 'array':
				return ArrayExecutable.parse(action);
			case 'id':
				return ActionIdExecutable.parse(action);
			case 'template':
				return ActionTemplateExecutable.parse(action);
			case 'npcSay':
				return NpcSayExecutable.parse(action);
			case 'teleport':
				return TeleportExecutable.parse(action);
			case 'heal':
				return HealExecutable.parse(action);
			case 'exp':
				return AddExpExecutable.parse(action);
			case 'monster':
				return AddMonsterExecutable.parse(action);
			case 'quest':
				return QuestExecutable.parse(action);
			case 'addItem':
				return AddItemExecutable.parse(action);
			case 'addPet':
				return AddPetExecutable.parse(action);
			case 'removeItem':
				return RemoveItemExecutable.parse(action);
			case 'gold':
				return AddGoldExecutable.parse(action);
			default:
				Logger.error('Action type not implemented', action);
				throw Error('Action type not implemented.');
		}
	}
}
