import { PetPackets } from '../../Responses/PetPackets';
import { PlayerPackets } from '../../Responses/PlayerPackets';
import { Random } from '../../Utils/Random';
import type { ClientActionContext } from '../GameActionContext';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionExp } from '../GameActionTypes';

/**
 * Give exp to the player or pet.
 */
export class AddExpExecutable extends GameActionExecutable<ClientActionContext> {
	protected constructor(protected override readonly action: GameActionExp) {
		super(action);
	}

	public static parse(action: GameActionExp): AddExpExecutable {
		return new this(action);
	}

	protected run(context: ClientActionContext): void {
		let amount = Random.fromJson(this.action.amount);

		if (this.action.pet) this.runPet(context, amount);
		else this.runPlayer(context, amount);
	}

	private runPlayer({ client, player }: ClientActionContext, amount: number): void {
		let levels = player.level.addExp(amount);

		if (levels === 0) client.write(PlayerPackets.experience(player));
		else {
			player.fightData.stats.updateStatPointsForLevel(player.level.level);
			client.write(PlayerPackets.level(player));
		}
	}

	private runPet({ client, player }: ClientActionContext, amount: number): void {
		let pet = player.activePet;

		if (!pet) return;

		let levels = pet.level.addExp(amount);

		if (levels === 0) client.write(PetPackets.experience(pet));
		else {
			pet.fightData.stats.updateStatPointsForLevel(pet.level.level);
			client.write(PetPackets.level(pet));
		}
	}
}
