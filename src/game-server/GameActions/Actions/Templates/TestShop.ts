import { Item } from '../../../GameState/Item/Item';
import { ShopPackets } from '../../../Responses/ShopPackets';
import type { ActionTemplateCallback } from '../ActionTemplateExecutable';

export const testShop: ActionTemplateCallback = ({ client, game, player }) => {
	let item = new Item(game.baseItems.get(2)!);
	item.price = 50;

	let item2 = new Item(game.baseItems.get(3)!);
	item2.price = 100;

	let items = [item, item2];
	player.memory.npcItems = items;
	client.write(...ShopPackets.npcVendor(items));
};
