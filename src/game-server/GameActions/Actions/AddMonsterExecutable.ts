import { getConfig } from '../../Config/Config';
import { Monster } from '../../GameState/Monster/Monster';
import type { Player } from '../../GameState/Player/Player';
import { MessagePackets } from '../../Responses/MessagePackets';
import { PetPackets } from '../../Responses/PetPackets';
import { PlayerPackets } from '../../Responses/PlayerPackets';
import type { ClientActionContext } from '../GameActionContext';
import { GameActionExecutable } from '../GameActionExecutable';
import type { GameActionMonster } from '../GameActionTypes';

/**
 * Calculate how much exp to give based on level of monster, expBase and player level
 * If level difference between monster and player is to great then it will only reward 144 exp
 * @param player
 * @returns
 */
export function calculateMonsterExp(player: Player): number {
	let exp = 0;

	if (player.fightData.currentFight) {
		for (let monster of player.fightData.currentFight.sideB) {
			if (!(monster.base instanceof Monster)) continue;
			// TODO Generic formula
			let tempExp = monster.base.rewards?.expBase ?? 0 * monster.base.level;
			if (Math.abs(monster.base.level - player.level.level) > 10 && tempExp > 144) {
				tempExp = 144;
			}
			exp += tempExp;
		}

		if (!player.fightData.currentFight.checkWinner(player))
			// Deduct exp if player loses fight
			exp = -exp * 0.1;
	}

	return exp * getConfig().modifiers.exp;
}

/**
 * Calculate how much gold to give based on level of monster, goldBase and player level
 * If level difference between monster and player is to great then it will only reward 144 gold
 * @param player
 * @returns
 */
export function calculateMonsterGold(player: Player): number {
	let gold = 0;

	// Only distribute gold if winner
	if (player.fightData.currentFight?.checkWinner(player)) {
		if (player.fightData.currentFight) {
			for (let monster of player.fightData.currentFight.sideB) {
				if (!(monster.base instanceof Monster)) continue;
				// TODO Generic formula
				let tempGold = monster.base.rewards?.goldBase ?? 0 * monster.base.level;
				if (Math.abs(monster.base.level - player.level.level) > 10 && tempGold > 144) {
					tempGold = 144;
				}
				gold += tempGold;
			}
		}
	}

	return gold * getConfig().modifiers.gold;
}

/**
 * Give exp and gold to the player and pet from monster kills.
 */
export class AddMonsterExecutable extends GameActionExecutable<ClientActionContext> {
	protected constructor(protected override readonly action: GameActionMonster) {
		super(action);
	}

	public static parse(action: GameActionMonster): AddMonsterExecutable {
		return new this(action);
	}

	protected run(context: ClientActionContext): void {
		// Floor exp/gold values to prevent long floating point numbers
		let gold = Math.floor(calculateMonsterGold(context.player));
		let exp = Math.floor(calculateMonsterExp(context.player));
		this.addGold(context.player, gold);
		this.addExp(context, exp);

		// Message player about their exp and gold gains
		let message: string | null = null;

		if (exp !== 0) {
			message = `${exp} exp`;
		}

		if (gold !== 0) {
			if (message) message = `${message} and ${gold} gold`;
			else message = `${gold} gold`;
		}

		// Message the player about their reward
		if (message)
			context.player.client?.write(MessagePackets.showMessage(`Recieved ${message}`));
	}

	/**
	 * Distribute gold to the player
	 * @param player
	 */
	private addGold(player: Player, gold: number): void {
		if (gold !== 0) player.items.addGoldAndSend(gold);
	}

	/**
	 * Distribute exp to the player and pet
	 * @param context
	 */
	private addExp({ client, player }: ClientActionContext, exp: number): void {
		if (exp !== 0) {
			let pet = player.activePet;
			let petExp = 0;

			// If using a pet then add exp to it
			if (pet) {
				petExp = exp * 2;

				let petLevels = pet.level.addExp(petExp);

				if (petLevels === 0) {
					client.write(PetPackets.experience(pet));
				} else {
					pet.fightData.stats.updateStatPointsForLevel(pet.level.level);
					client.write(PetPackets.level(pet));
				}
			}

			// Add exp to the player
			let playerLevels = player.level.addExp(exp);

			if (playerLevels === 0) {
				client.write(PlayerPackets.experience(player));
			} else {
				player.fightData.stats.updateStatPointsForLevel(player.level.level);
				client.write(PlayerPackets.level(player));
			}
		}
	}
}
