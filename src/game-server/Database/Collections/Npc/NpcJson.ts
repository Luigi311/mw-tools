import type { Direction } from '../../../Enums/Direction';
import type { GameAction } from '../../../GameActions/GameActionTypes';
import type { PointJson } from '../../../Utils/Point';

export type NpcJson = {
	id: number;
	name: string;
	file: number;
	map: number;
	point: PointJson;
	direction: Direction;
	action?: GameAction;
};
