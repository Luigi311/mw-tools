export type UserJson = {
	username: string;
	password: string | null;
};
