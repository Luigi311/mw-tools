import type { NpcJson } from '../../../Database/Collections/Npc/NpcJson';
import { Direction } from '../../../Enums/Direction';

export const badlandsNpcsData: NpcJson[] = [
	{
		id: 0x80000332,
		name: 'Cursed Mage',
		file: 159,
		map: 15,
		point: { x: 2640, y: 2920 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000333,
		name: 'Teleporter',
		file: 159,
		map: 15,
		point: { x: 320, y: 3752 },
		direction: Direction.SouthWest,
	},
	{
		id: 0x80000340,
		name: 'Vast Mine',
		file: 144,
		map: 15,
		point: { x: 4752, y: 519 },
		direction: Direction.SouthWest,
		action: {
			type: 'teleport',
			targetNpcId: 0x80000342,
		},
	},
];
