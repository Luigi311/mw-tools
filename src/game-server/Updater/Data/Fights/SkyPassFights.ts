import { monsterTemplates } from '../../../Data/MonsterTemplates';
import { Fight } from '../../../GameState/Fight/Fight';
import { FightCreator } from '../../../GameState/Fight/FightCreator';
import type { FightMemberBase } from '../../../GameState/Fight/FightMember';
import { Monster } from '../../../GameState/Monster/Monster';
import { MonsterCreator } from '../../../GameState/Monster/MonsterCreator';
import type { ActionTemplateCallback } from '../../../GameActions/Actions/ActionTemplateExecutable';
import { FightType } from '../../../Enums/FightType';

export const battleInstructorFight: ActionTemplateCallback = ({ player, game }) => {
	let allies: FightMemberBase[] = FightCreator.getParticipants(player);
	let monsters: FightMemberBase[] = [];

	for (let i = 0; i < 4; ++i) {
		let monster2 = new Monster(MonsterCreator.create(monsterTemplates.greenCapilla));
		monster2.id += i;
		monsters.push(monster2);
	}
	let monster = new Monster(MonsterCreator.create(monsterTemplates.battleInstructor));
	monsters.push(monster);

	let fight = new Fight(game, allies, monsters, FightType.Monster);
	fight.start();
};
