import { getConfig } from '../Config/Config';
import { Packet } from '../PacketBuilder';
import { PacketType } from '../PacketType';

export abstract class ChatPackets {
	private static _serverInfo: Packet | null = null;

	/**
	 * Sent after picking a character.
	 * Contains the ip/port of the chat server.
	 */
	public static get serverInfo(): Packet {
		if (this._serverInfo === null) {
			let { chat } = getConfig();
			let ip = chat.ip.split('.').map(s => Number.parseInt(s));
			let port = chat.port;

			this._serverInfo = new Packet(22, PacketType.ChatServerInfo)
				.uint8(16, ip[0])
				.uint8(17, ip[1])
				.uint8(18, ip[2])
				.uint8(19, ip[3])
				.uint16(20, port);
		}

		return this._serverInfo;
	}

	/**
	 * A local chat message.
	 * @param sender
	 * @param message
	 */
	public static local(sender: number, message: string): Packet {
		return new Packet(16 + message.length, PacketType.ChatLocalBS)
			.uint16(6, message.length)
			.uint32(12, sender)
			.string(16, message);
	}

	/**
	 * A world chat message.
	 * @param sender
	 * @param message
	 */
	public static world(sender: number, name: string, message: string): Packet {
		return new Packet(24 + name.length + message.length, PacketType.ChatWorldBS)
			.uint16(6, 8 + name.length + message.length)
			.uint32(16, sender)
			.uint8(20, name.length)
			.uint8(21, message.length)
			.string(24, name)
			.string(24 + name.length, message);
	}
}
