import type { Player } from '../GameState/Player/Player';
import { Packet } from '../PacketBuilder';
import { PacketType } from '../PacketType';
import { AttrsStruct } from './Structs/AttrsStruct';
import { ExpStruct } from './Structs/ExpStruct';
import { PlayerMiscStruct } from './Structs/PlayerMiscStruct';
import { PlayerSkillStruct } from './Structs/PlayerSkillStruct';
import { PlayerStatsStruct } from './Structs/PlayerStatsStruct';
import { ResistStruct } from './Structs/ResistStruct';

export abstract class PlayerPackets {
	/**
	 * Contains most information for the stats screen.
	 * @param player
	 */
	public static information(player: Player): Packet {
		let packet = new Packet(168, PacketType.PlayerInformation).uint16(6, 152);

		if (player.guild) packet.string(16, player.guild.name, 14);

		return packet
			.struct(32, AttrsStruct, player)
			.struct(60, ExpStruct, player.level)
			.uint32(72, player.file)
			.struct(76, PlayerStatsStruct, player)
			.struct(144, PlayerMiscStruct, player);
	}

	/**
	 * Result of using stats, updates attributes.
	 * @param player
	 */
	public static useStats(player: Player): Packet {
		return new Packet(60, PacketType.PlayerUseStats)
			.uint16(6, 44)
			.uint32(12, 1) // 1 = success, otherwise error
			.struct(16, AttrsStruct, player)
			.struct(44, ExpStruct, player.level)
			.uint32(56, player.file);
	}

	/**
	 * Used when player level changes, updates stats, exp.
	 * @param player
	 */
	public static level(player: Player): Packet {
		return new Packet(128, PacketType.PlayerLevel)
			.uint16(6, 112)
			.struct(16, PlayerStatsStruct, player)
			.struct(84, AttrsStruct, player)
			.struct(112, ExpStruct, player.level)
			.uint32(124, player.file);
	}

	/**
	 * Used when player exp changes.
	 * @param player
	 */
	public static experience(player: Player): Packet {
		return new Packet(28, PacketType.PlayerExperience)
			.uint16(6, 12)
			.struct(16, ExpStruct, player.level);
	}

	/**
	 * Sends the player's resist data.
	 * @param player
	 */
	public static resist(player: Player): Packet {
		return new Packet(76, PacketType.PlayerResist)
			.uint16(6, 60)
			.struct(16, ResistStruct, player.fightData.resist);
	}

	/**
	 * Updates player misc stats (reputation, etc.).
	 * @param player
	 */
	public static misc(player: Player): Packet {
		return new Packet(40, PacketType.PlayerMisc)
			.uint16(6, 24)
			.struct(16, PlayerMiscStruct, player);
	}

	/**
	 * Updates the player skill data.
	 * @param player
	 */
	public static skills(player: Player): Packet {
		let skills = player.fightData.skills.skillData;
		let packet = new Packet(16 + skills.length * 10, PacketType.PlayerSkills).uint32(
			12,
			skills.length,
		);

		for (let i = 0; i < skills.length; ++i)
			packet.struct(16 + i * 10, PlayerSkillStruct, skills[i]);

		return packet;
	}

	/**
	 * Updates the player's hp, will show an animation.
	 * @param currentHp
	 */
	public static healHp(currentHp: number): Packet {
		return new Packet(16, PacketType.PlayerHealHp).uint32(12, currentHp);
	}

	/**
	 * Updates the player's mp, will show an animation.
	 * @param currentMp
	 */
	public static healMp(currentMp: number): Packet {
		return new Packet(16, PacketType.PlayerHealMp).uint32(12, currentMp);
	}
}
