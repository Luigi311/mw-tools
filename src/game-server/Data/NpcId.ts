export const enum NpcId {
	WoodTele = 0x80000001,
	BlythonTele = 0x80000002,
	DescityTele = 0x80000004,
	TestQuestA = 0x80001000,
	TestQuestB = 0x80001001,
	TestQuestC = 0x80001002,
}
