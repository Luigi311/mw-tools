import { ItemListType } from '../Enums/ItemListType';
import type { Item } from '../GameState/Item/Item';
import { Logger } from '../Logger/Logger';
import { getPacketType } from '../PacketReader';
import { PacketType } from '../PacketType';
import { ItemPackets } from '../Responses/ItemPackets';
import type { PlayerConnection } from '../Server/Game/GameConnection';
import type { PacketConnection } from '../Server/Packet/PacketConnection';
import { AbstractPacketHandler } from './AbstractPacketHandler';

/**
 * Handles packets regarding the inventory.
 */
export class InventoryPacketHandler extends AbstractPacketHandler {
	/**
	 * Checks if this packet handler is used to handle a packet type.
	 * @param type
	 */
	public handlesType(type: PacketType): boolean {
		return type >= 0x00070000 && type < 0x00080000;
	}

	/**
	 * Handles the given packet.
	 * @param packet
	 * @param client
	 */
	public handlePacket(packet: Buffer, client: PacketConnection): void {
		if (!this.hasPlayer(client)) return;

		let type = getPacketType(packet);

		switch (type) {
			case PacketType.InventoryData:
				client.write(
					ItemPackets.inventory(client.player.items.inventory),
					ItemPackets.equipment(client.player.items.equipment),
				);
				break;
			case PacketType.ItemRemove:
				this.onItemRemove(packet, client);
				break;
			case PacketType.ItemChange:
				this.onItemChange(packet, client);
				break;
			case PacketType.ItemUse:
				this.onItemUse(packet, client);
				break;
			case PacketType.GoldData:
				client.write(ItemPackets.gold(client.player.items));
				break;
			case PacketType.ItemUnequip:
				this.onItemUnequip(packet, client);
				break;
			case PacketType.ItemInfo:
				this.onItemInfo(packet, client);
				break;
			default:
				this.notImplemented(packet);
		}
	}

	/**
	 * Client drops an item.
	 * @param packet
	 * @param client
	 */
	private onItemRemove(packet: Buffer, client: PlayerConnection): void {
		let index = packet.readUInt16LE(12);
		client.player.items.removeSlotAndSend(index);
	}

	/**
	 * Client moves an item.
	 * @param packet
	 * @param client
	 */
	private onItemChange(packet: Buffer, client: PlayerConnection): void {
		let srcIndex = packet.readUInt16LE(14);
		let dstIndex = packet.readUInt16LE(12);
		client.player.items.moveItemAndSend(srcIndex, dstIndex);
	}

	/**
	 * Client uses an item.
	 * @param packet
	 * @param client
	 */
	private onItemUse(packet: Buffer, client: PlayerConnection): void {
		let index = packet.readUInt16LE(12);
		client.player.items.useItemAndSend(index);
	}

	/**
	 * Client unequips an item.
	 * @param packet
	 * @param client
	 */
	private onItemUnequip(packet: Buffer, client: PlayerConnection): void {
		let index = packet.readUInt16LE(12);
		client.player.items.unequipItemAndSend(index);
	}

	/**
	 * Client requests the info for an item.
	 * @param packet
	 * @param client
	 */
	private onItemInfo(packet: Buffer, client: PlayerConnection): void {
		let index = packet.readUInt16LE(12);
		let type: ItemListType = packet.readUInt16LE(14);
		let item: Item | null = null;

		switch (type) {
			case ItemListType.Inventory:
				item = client.player.items.inventory.get(index);
				break;
			case ItemListType.Equipment:
				item = client.player.items.equipment.get(index);
				break;
			case ItemListType.NpcShop:
				item = client.player.memory.npcItems?.[index] ?? null;
				break;
			default:
				Logger.info('ItemInfo: Not implemented itemlist', type);
				break;
		}

		if (!item) return;

		client.write(ItemPackets.info(item));
	}
}
