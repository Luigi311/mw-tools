import { getPacketType } from '../PacketReader';
import { PacketType } from '../PacketType';
import { ChatPackets } from '../Responses/ChatPackets';
import { ChatConnection } from '../Server/Chat/ChatConnection';
import type { PacketConnection } from '../Server/Packet/PacketConnection';
import { endAtZero } from '../Utils/StringUtils';
import { AbstractPacketHandler } from './AbstractPacketHandler';

/**
 * Handles chat packets.
 */
export class ChatPacketHandler extends AbstractPacketHandler {
	/**
	 * Checks if this packet handler is used to handle a packet type.
	 * @param type
	 */
	public handlesType(type: PacketType): boolean {
		return type >= 0x00040074 && type < 0x00050000;
	}

	/**
	 * Handles the given packet.
	 * @param packet
	 * @param client
	 */
	public handlePacket(packet: Buffer, client: PacketConnection): void {
		if (!(client instanceof ChatConnection)) return;

		let type = getPacketType(packet);

		switch (type) {
			case PacketType.ChatLocalBC:
				this.onLocalChat(packet, client);
				break;
			//ChatPartyBC
			//ChatGuildBC
			//ChatMarketBC
			case PacketType.ChatWorldBC:
				this.onWorldChat(packet, client);
				break;
			//ChatPMBC
			//ChatGMBC
			default:
				this.notImplemented(packet);
		}
	}

	/**
	 * The client sent a local chat message.
	 * @param packet
	 * @param client
	 */
	private onLocalChat(packet: Buffer, client: ChatConnection): void {
		let sender = packet.readUInt32LE(12);
		if (sender !== client.player.id) return;

		let message = endAtZero(packet.toString('ascii', 16));
		client.channelWriter.local(ChatPackets.local(client.player.id, message));
	}

	/**
	 * The client sent a world chat message.
	 * @param packet
	 * @param client
	 */
	private onWorldChat(packet: Buffer, client: ChatConnection): void {
		let sender = packet.readUInt32LE(16);
		if (sender !== client.player.id) return;

		let message = endAtZero(packet.toString('ascii', 24));
		client.channelWriter.world(
			ChatPackets.world(client.player.id, client.player.name, message),
		);
	}
}
