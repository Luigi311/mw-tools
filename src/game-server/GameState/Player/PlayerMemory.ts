import type { ClientActionContext } from '../../GameActions/GameActionContext';
import type { GameActionExecutable } from '../../GameActions/GameActionExecutable';
import type { Item } from '../Item/Item';
import type { Npc } from '../Npc/Npc';

/**
 * Various caches used while the player is online.
 */
export class PlayerMemory {
	/**
	 * The npc the player is currently interacting with.
	 */
	public activeNpc: Npc | null = null;

	/**
	 * The options the activeNpc offered the player.
	 */
	public npcOptions: GameActionExecutable<ClientActionContext>[] | null = null;

	/**
	 * The items the activeNpc offered to sell to the player.
	 */
	public npcItems: Item[] | null = null;

	/**
	 * Used for starting random monster fights.
	 */
	public stepCount: number = 0;
}
