import type { BaseItem } from '../../Database/Collections/BaseItem/BaseItemTypes';
import { createClientContext } from '../../GameActions/GameActionContext';
import { Logger } from '../../Logger/Logger';
import { ItemEquipStatus, ItemPackets } from '../../Responses/ItemPackets';
import { MessagePackets } from '../../Responses/MessagePackets';
import { ItemContainer } from '../Item/ItemContainer';
import { ItemType } from '../Item/ItemType';
import type { Player } from './Player';

/**
 * Manages the player's inventory, equipment, bank and gold.
 */
export class PlayerItems {
	/**
	 * How much gold the player has in their inventory.
	 */
	public gold: number = 0;

	/**
	 * How much gold the player has deposited.
	 */
	public bankGold: number = 0;

	/**
	 * The inventory items.
	 */
	public inventory: ItemContainer = new ItemContainer(40);

	/**
	 * The bank items.
	 */
	public bank: ItemContainer = new ItemContainer(40);

	/**
	 * Equipped items.
	 */
	public equipment: ItemContainer = new ItemContainer(7);

	public constructor(private player: Player) {}

	/**
	 * Returns the number of items the player has with the given id.
	 * Returns total count, not slots used.
	 * @param baseItemId
	 * @param includeBank
	 */
	public getItemCount(baseItemId: number, includeBank: boolean = false): number {
		let count = this.inventory.getItemCount(baseItemId);

		if (includeBank) count += this.bank.getItemCount(baseItemId);

		return count;
	}

	/**
	 * Add the given number of items to the inventory.
	 * @param baseItem
	 * @param count
	 */
	public addItemAndSend(baseItem: BaseItem, count: number): void {
		let changed = this.inventory.addItem(baseItem, count);
		this.player.client?.write(
			ItemPackets.change(changed),
			MessagePackets.showMessage('A new item was added!'),
		);
	}

	/**
	 * Removes the item at the given slot.
	 * @param index
	 */
	public removeSlotAndSend(index: number): void {
		let item = this.inventory.get(index);

		if (item === null || item.locked || item.questItem) return;

		this.inventory.delete(index);
		this.player.client?.write(ItemPackets.remove(index));
	}

	/**
	 * Removes the first item(s) in the inventory matching this base item.
	 * @param baseItemId
	 * @param count
	 */
	public removeItemAndSend(baseItemId: number, count: number = 1): void {
		let changed = this.inventory.removeItem(baseItemId, count);
		this.player.client?.write(ItemPackets.change(changed));
	}

	/**
	 * Moves or restacks items, or switch them around.
	 * @param srcIndex
	 * @param dstIndex
	 */
	public moveItemAndSend(srcIndex: number, dstIndex: number): void {
		let changed = this.inventory.moveItem(srcIndex, dstIndex);
		this.player.client?.write(ItemPackets.change(changed));
	}

	/**
	 * Adds gold to the inventory.
	 * @param gold can be negative to remove gold.
	 */
	public addGoldAndSend(gold: number): void {
		this.gold += gold;
		this.player.client?.write(ItemPackets.gold(this));
	}

	/**
	 * Use the item at the given inventory slot.
	 * @param index
	 */
	public useItemAndSend(index: number): void {
		let item = this.inventory.get(index);

		if (!this.player.client || !item || item.count <= 0) return;

		switch (item.type) {
			case ItemType.None:
				return;
			case ItemType.Equipment:
				this.equipItemAndSend(index);
				return;
			case ItemType.Consumable:
				this.consumeItemAndSend(index);
				return;
			case ItemType.Usable:
				item.action?.execute(createClientContext(this.player.client));
				return;
			default:
				Logger.error('Item has unknown type', item);
				throw Error('Item has unknown type ' + item.type);
		}
	}

	/**
	 * Move an item from equipment to inventory. Does nothing when there is no space.
	 * @param index
	 */
	public unequipItemAndSend(index: number): void {
		let item = this.equipment.get(index);

		if (item === null) return;

		let dst = this.inventory.getFreeIndex();

		if (dst === null) return;

		this.inventory.set(dst, item);
		this.equipment.delete(index);

		this.player.client?.write(
			ItemPackets.change([[dst, item]]),
			ItemPackets.equip(ItemEquipStatus.Ok, index, null),
		);
	}

	/**
	 * Equips an item. If an item is already equipped they will switch places.
	 * @param index
	 */
	private equipItemAndSend(index: number): void {
		let item = this.inventory.get(index);

		if (item === null || item.equipmentSlot === null) return;

		let dstIndex = item.equipmentSlot;
		let dstItem = this.equipment.get(dstIndex);
		this.equipment.set(dstIndex, item, true);

		if (dstItem) this.inventory.set(index, dstItem, true);
		else this.inventory.delete(index);

		this.player.client?.write(
			ItemPackets.change([[index, dstItem]]),
			ItemPackets.equip(ItemEquipStatus.Ok, dstIndex, item),
		);
	}

	/**
	 * Use the item and reduce its count by one. Removes the item if none are left.
	 * @param index
	 */
	private consumeItemAndSend(index: number): void {
		let item = this.inventory.get(index);

		if (!this.player.client || !item) return;

		let allowed = item.action?.execute(createClientContext(this.player.client)) ?? false;

		if (!allowed) return;

		--item.count;

		if (item.count === 0) {
			this.inventory.delete(index);
			item = null;
		}

		this.player.client.write(ItemPackets.change([[index, item]]));
	}
}
